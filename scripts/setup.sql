
--  Copyright 2010-2012 Stuart Prescott <stuart@debian.org>
--
--  infobot-web is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  infobot-web is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program; if not, see <http://www.gnu.org/licenses/>.

SET NAMES utf8;

DROP DATABASE IF EXISTS dpkg;
CREATE DATABASE dpkg DEFAULT CHARACTER SET utf8;

USE dpkg;

DROP TABLE IF EXISTS factoids_history;
CREATE TABLE factoids_history (
  factoid_key VARCHAR(64) NOT NULL default '',
  requested_by VARCHAR(80) DEFAULT NULL,
  requested_time INT(11) DEFAULT NULL,
  requested_count SMALLINT(5) UNSIGNED DEFAULT NULL,
  created_by VARCHAR(80) DEFAULT NULL,
  created_time INT(11) DEFAULT '0',
  modified_by VARCHAR(80) DEFAULT NULL,
  modified_time INT(11) DEFAULT NULL,
  locked_by VARCHAR(80) DEFAULT NULL,
  locked_time INT(11) DEFAULT NULL,
  factoid_value TEXT NOT NULL,
  history_time INT(11) DEFAULT NULL,
  PRIMARY KEY (factoid_key, history_time)
);

DROP TABLE IF EXISTS factoids_suggestions;
CREATE TABLE factoids_suggestions (
  factoid_key VARCHAR(64) NOT NULL DEFAULT '',
  suggested_by VARCHAR(80) DEFAULT NULL,
  suggested_time INT(11) DEFAULT NULL,
  suggested_action VARCHAR(16) DEFAULT NULL,
  suggested_value TEXT DEFAULT NULL,
  INDEX (factoid_key)
);

USE mysql;

DELETE FROM user WHERE User='infobotuser';
INSERT INTO user (Host,User,Password,Reload_priv) VALUES ('localhost','infobotuser',PASSWORD('infobotpass'),'N');
FLUSH PRIVILEGES;
GRANT SELECT ON dpkg.* TO infobotuser@localhost;
GRANT INSERT,UPDATE,DELETE ON dpkg.factoids_suggestions TO infobotuser@localhost;
FLUSH PRIVILEGES;
GRANT CREATE,DROP,ALTER,INDEX,LOCK TABLES,SELECT,INSERT,UPDATE,DELETE ON dpkg.* TO cronuser@localhost;
FLUSH PRIVILEGES;

