#!/bin/sh
#
# Copyright 2010-2012 Stuart Prescott <stuart@debian.org>
#
# infobot-web is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# infobot-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.
#

# Assumes that that user that this is running as can log in to mysql
# without specifying a password -- i.e. either through mysql permissions
# for that user on localhost, or with a ~/.my.cnf

set -e
set -u

#BASE=/srv/www/ircbots.debian.net
BASE=${BASE:-$(dirname "$(readlink -f $0)")/..}

if [ ! -d "$BASE/sql" -o ! -d "$BASE/scripts" -o ! -d "$BASE/website" ]
then
  echo "Couldn't find location; please set BASE."
  exit 1
fi

NET=true
DUMPS=$BASE/sql/
DUMP=$DUMPS/dpkg_dump.gz
INCREMENTAL=${INCREMENTAL:-true}

# If a command line argument is given, use that as the dump file instead
if [ $# -eq 1 ]; then
  NET=false
  DUMP=$1
  echo "Importing $1"
elif [ $# -gt 1 ]; then
  INCREMENTAL=false "$0" "$1"
  shift
  for i in "$@"; do
    "$0" "$i"
  done
  exit
fi


if $NET; then
  OLDMD5=$(md5sum $DUMP)
  wget -q -O $DUMP https://dpkg.donarmstrong.com/apt_dump.gz
  SIZE=$(stat -c%s $DUMP)
  if [ $SIZE -lt 1000000 ]; then
    echo "Truncated db dump. Exiting."
    exit 1
  fi
  NEWMD5=$(md5sum $DUMP)
  if [ "$OLDMD5" = "$NEWMD5" ]; then
    echo "Unchanged db dump. Exiting."
    exit 0
  fi
fi

CONFIG=$BASE/website/conf/db.php
conf() {
  sed -nr "s/\\\$$1[[:space:]]*=[[:space:]]*(['\"])(.*)\1.*/\2/p" $CONFIG
}

if $INCREMENTAL; then
  echo 'DROP TABLE IF EXISTS factoids_tmp;
        RENAME TABLE factoids TO factoids_tmp' |
      mysql "$(conf db_name)"
fi

# When importing as latin1 there are no key clashes (but the data is
# actually mostly utf-8). Importing with a non-binary utf-8 collation gives
# key clashes if PRIMARY KEY is used but not if INDEX is used.

zcat $DUMP |
    sed 's/PRIMARY KEY[[:space:]]*("factoid_key")/INDEX ("factoid_key")/; s/CMD: /cmd: /g' |
    mysql --default-character-set=utf8 \
            "$(conf db_name)"

if $INCREMENTAL; then
  DUMPTIME=$(date -d "$(zcat $DUMP | tail -1 | cut -f 5- -d\ )" +%s)

  echo "
    REPLACE INTO factoids_history
          (factoid_key, factoid_value,
            requested_time, requested_count,
            created_by, created_time, modified_by, modified_time,
            locked_by, locked_time, history_time)
      SELECT o.factoid_key, o.factoid_value,
              o.requested_time, o.requested_count,
              o.created_by, o.created_time, o.modified_by, o.modified_time,
              o.locked_by, o.locked_time, $DUMPTIME
        FROM factoids
        LEFT JOIN factoids_tmp AS o
          ON factoids.factoid_key = o.factoid_key
        WHERE factoids.factoid_value != o.factoid_value;" |
      mysql --default-character-set=utf8 "$(conf db_name)"

  echo 'DROP TABLE factoids_tmp;' | mysql "$(conf db_name)"
fi
