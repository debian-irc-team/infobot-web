<?php
/*
 Copyright 2012 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

// error_reporting(E_STRICT | E_ALL);

require_once "inc/utils.php";
require_once "theme/pageheader.php";
require_once "theme/pagefooter.php";
require_once "inc/factoids.php";
require_once 'inc/nav.php';
require_once 'inc/suggestions.php';
require_once 'inc/suggestionlist.php';
require_once 'inc/site-info.php';


$action = safe_index($_REQUEST, 'action');

switch ($action) {
  case "prop_del":
    $title = "Suggest factoid deletion";
    $run = 'prop_del';
    break;
  case "prop_change":
    $title = "Suggest factoid value change";
    $run = 'prop_change';
    break;
  case "review_del":
    $title = "Review deletion suggestions";
    $run = 'review_del';
    break;
  case "review_change":
    $title = "Review change suggestions";
    $run = 'review_change';
    break;
  default:
    $title = "Suggestions";
    $run = 'show_review';
}
$description = $title;

showPageHeader($title, $csslist);
include "theme/header.html";

showBreadcrumbs("suggestions", $description);

print "<div class='suggestions'>";
if ($run) {
  call_user_func($run, $suggestions);
}
print "</div>";

include "theme/footer.html";
showPageFooter();



function show_review($suggestions) {
  $currurl = parse_url($_SERVER['REQUEST_URI']);

  print "<h2>Review actions</h2>\n<ul>\n";
  printf("<li><a href='%s?%s'>Review deletions</a> (%d pending)</li>",
             $currurl['path'],
             http_build_query(array('action' => 'review_del'), '', '&amp;'),
            $suggestions->count(SuggestionType::PROP_DEL));
//   printf("<li><a href='%s?%s'>Review changes</a> (%d pending)</li>",
//              $currurl['path'],
//              http_build_query(array('action' => 'review_change'), '', '&amp;'),
//             $suggestions->count(SuggestionType::PROP_CHANGE));
  print "</ul>\n";
}

function review_value($suggestions) {
  error_log("prop_value no implemented");
}

function review_del($suggestions) {
  global $factoids;
  $format = safe_index($_REQUEST, 'format', 'html');
  print "<h2>Suggested factoid deletions</h2>\n";
  $results = $suggestions->suggestionsForFactoid();
  if ($format == 'bot') {
    showPropDelList($results, $format);
  } else {
    $keys = array();
    foreach($results as $f) {
      array_push($keys, $f->_factoid_key);
    }
    $results = $factoids->getFactoidsByKeyList($keys);
    showPropDelList($results, $format);

    if (showBotDelLink()) {
      $currurl = parse_url($_SERVER['REQUEST_URI']);
      printf("<a href='%s?%s'>make delete script</a><br /><br />\n",
                $currurl['path'],
                http_build_query(
                  array('action' => 'review_del',
                        'format' => 'bot'), '', '&amp;'));

    }
  }
}

function prop_value($suggestions) {
  error_log("prop_value no implemented");
}

function prop_del($suggestions) {
  $slist = $_POST['del'];
  foreach($slist as $key) {
    $s = new Suggestion();
    $s->propose_deletion($key);
    $suggestions->createSuggestionForFactoid($s);
  }
  print "<h2>Suggest factoid deletion</h2>
          <p>Suggestions recorded, thank you.</p>";
}

