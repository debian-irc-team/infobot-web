<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2009 Alexander Tait Brotman
 Copyright 2010 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

// error_reporting(E_STRICT | E_ALL);

require_once "theme/pageheader.php";
require_once "theme/pagefooter.php";
require_once "inc/factoids.php";
require_once "inc/factoidlist.php";
require_once 'inc/nav.php';
require_once 'inc/utils.php';
require_once 'inc/site-info.php';

$query = safe_index($_REQUEST, 'key');
if ($query)
{
	$factoid = $factoids->getFactoidByKey( $query );
}
else
{
	header("Location: http:search.php");
	return;
}
if( ! $factoid )
{
	header("Location: http:search.php");
	return;
}

$title = sprintf("Factoid '%s'", htmlentities($query, ENT_QUOTES, "UTF-8"));
showPageHeader($title, $csslist);
include "theme/header.html";

$options = getOptions();
showBreadcrumbs("search", $query);
showForm("key", $query, $options);

$quality = new FactoidQuality($factoid);

?>
<br />
<table class="factoid">
<tr>
	<th>Factoid Key</th>
	<td><?php echo htmlentities($factoid->_factoid_key, ENT_QUOTES, "UTF-8"); ?></td>
</tr>
<tr>
	<th>Value</th>
	<td><?php echo $factoid->getFactoidValue() ?></td>
</tr>
<?php
$aliases = array();
$currval = $factoid->_factoid_value;
while (preg_match('/^<reply>\s*see\s+(.{1,30})$/i', $currval, $matches)) {
  $target = $matches[1];
  // prevent loops
  if (array_key_exists($target, $aliases)) {
    break;
  }
  $nextfactoid = $factoids->getFactoidByKey($target);
  if ($nextfactoid) {
    print "<tr>
      <th>Aliased to</th>
      <td>".$nextfactoid->getFactoidValue()."</td>
    </tr>\n";
    $aliases[$target] = 1;
    $currval = $nextfactoid->_factoid_value;
  } else {
    break;
  }
}

?>
<tr>
	<th>Author</th>
	<td><?php echo $factoid->getFactoidCreator() ?></td>
</tr>
<tr>
	<th>Creation Date</th>
	<td><?php echo $factoid->getCreatedDate() ?></td>
</tr>
<tr>
	<th>Last Modified By</th>
	<td><?php echo $factoid->getFactoidModifier() ?></td>
</tr>
<tr>
	<th>Last&nbsp;Modified&nbsp;Date</th>
	<td><?php echo $factoid->getModifiedDate() ?></td>
</tr>
<tr>
        <th>Requested</th>
        <td><?php echo $factoid->_requested_count ?></td>
</tr>
<tr>
        <th>Length</th>
        <td><?php
            printf("<span class='%s'>%d bytes / %d characters</span>",
              $quality->lengthStyle(),
              $factoid->getFactoidLength(false),
              $factoid->getFactoidLength(true)) ?></td>
</tr>
<?php
$aliases = $factoids->getFactoidsByRegex(
        sprintf("^<reply>[[:space:]]*see[[:space:]]+%s$", preg_quote($query)),
        $options);
if ($aliases) {
  print "<tr><th>Aliases</th><td>";
  foreach ($aliases as $a) {
    if (! preg_match("/#del#/i", $a->_factoid_key)) {
      printf("<a href='?key=%s'>%s</a>\n",
              urlencode($a->_factoid_key),
              htmlentities($a->_factoid_key, ENT_QUOTES, "UTF-8"));
    }
  }
  print "</td></tr>\n";
}
$xrefs = $factoids->getFactoidsByRegex(
        sprintf("\<%s\>", preg_quote($query)),
        $options);
if ($xrefs) {
  print "<tr><th>Cross-references</th><td>";
  foreach ($xrefs as $xr) {
    printf("<a href='?key=%s'>%s</a>\n",
            urlencode($xr->_factoid_key),
            htmlentities($xr->_factoid_key, ENT_QUOTES, "UTF-8"));
  }
  print "</td></tr>\n";
}
?>
</table>
<br />
<?php

if (showPropDel()) {
  printf("<form method='post' action='suggest.php'>
        <input type='hidden' name='action' value='prop_del' />
        <input type='hidden' name='del[]' value='%s' />
        <input type='submit' name='submit' value='Suggest deletion' />
        </form>",
        htmlentities($factoid->_factoid_key, ENT_QUOTES, "UTF-8"));
}

$history = safe_index($_REQUEST, 'history');
if (! $history) $history = showHistory();
if ($history) {
  $historylist = $factoids->getFactoidHistory($factoid->_factoid_key, $history);
  array_unshift($historylist, $factoid);
  print "<h2>Factoid history</h2>\n";
  print "Current value shown first; note that factoids may have been altered
          due to inappropriate or inaccurate content.\n";
  showFactoidHistoryList($historylist);
} else {
  printf("<a href='?key=%s&amp;history=%d'>Show recent factoid history</a><br />
          Note that factoids may have been altered due to inappropriate or
          inaccurate content.\n<br /><br />",
          urlencode($factoid->_factoid_key),
          30);
}

include "theme/footer.html";
showPageFooter();

