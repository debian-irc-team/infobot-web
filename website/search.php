<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2009 Alexander Tait Brotman
 Copyright 2010 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

// error_reporting(E_STRICT | E_ALL);

require_once "inc/utils.php";
require_once "theme/pageheader.php";
require_once "theme/pagefooter.php";
require_once "inc/factoids.php";
require_once "inc/factoidlist.php";
require_once 'inc/nav.php';
require_once 'inc/site-info.php';

$query = safe_index($_REQUEST, 'q');
$term  = safe_index($_REQUEST, 'term');

if (! $query && safe_index($_REQUEST, 'author')) {
  $term = 'author';
  $query = $_REQUEST['author'];
} elseif (! $query && safe_index($_REQUEST, 'value')) {
  $term = 'value';
  $query = $_REQUEST['value'];
} elseif (! $query && safe_index($_REQUEST, 'key')) {
  $term = 'key';
  $query = $_REQUEST['key'];
}

$options = getOptions();
$results = false;

if ($query) {
  if( $term == 'value' ) {
    $results = $factoids->getFactoidsbyValue( $query , $options );
  } elseif( $term == 'author' ) {
    $results = $factoids->getFactoidsbyAuthor( $query , $options );
  } else {
    $results = $factoids->getFactoidsbyKey( $query , $options );
  }
}

if (! $results) {
  $title = "Search the bot!";
} else {
  $title = sprintf("Bot search - found %d search %s for %s",
                          count($results),
                          count($results)==1 ? "result" : "results",
                          htmlentities($query, ENT_QUOTES, "UTF-8"));
}

showPageHeader($title, $csslist);
include "theme/header.html";

showBreadcrumbs("search", $term);
showForm($term, $query, $options);


if( $results )
{
  $results_num_rows = count($results);

  $currurl = parse_url($_SERVER['REQUEST_URI']);

  $args = array(
                  'term'     => $term,
                  'q'        => $query,
                  'reverse'  => $options['reverse'],
                  'nowild'   => ! $options['wild'],
                  'noclean'  => ! $options['clean'],
                  'days'     => $options['days']
                );
  $self_url = sprintf("%s?%s", $currurl['path'], http_build_query($args, '', '&amp;'));

  $revnext = $options['reverse'] ? 'n' : 'y';
  $args = array(
                  'term'     => $term,
                  'q'        => $query,
                  'sort'     => $options['sort'],
                  'reverse'  => $revnext,
                  'nowild'   => ! $options['wild'],
                  'noclean'  => ! $options['clean'],
                  'days'     => $options['days']
                );
  $self_url_reverse = sprintf("%s?%s", $currurl['path'], http_build_query($args, '', '&amp;'));

  echo "<h1>Results ($results_num_rows".($options['days']? " in last {$options['days']} days" : "").")</h1>
    <div style='text-align:center;'>
    Sorting:
      <a href='$self_url&amp;sort=key'>key</a> |
      <a href='$self_url&amp;sort=age'>last modified</a> |
      <a href='$self_url&amp;sort=value'>value</a> |
      <a href='$self_url_reverse'>reverse</a>
    </div>";

  showFactoidList($results, autoDisplayOptions());

} elseif ($query) {
  echo "<b>No results</b><hr style='margin-top: 1em; margin-bottom: 1em;' />";
} else {
  showWelcomeMessage();
}

include "theme/footer.html";
showPageFooter();
