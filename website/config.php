<?php
/*
 Copyright 2012 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

// error_reporting(E_STRICT | E_ALL);

require_once "inc/utils.php";
require_once "theme/pageheader.php";
require_once "theme/pagefooter.php";
require_once 'inc/site-info.php';

$action = safe_index($_REQUEST, 'action', false);
if ($action) {
  $endtime = time()+10*365*24*60*60;
  setcookie("prop_del", safe_index($_POST, 'prop_del'), $endtime);
  setcookie("botlink",  safe_index($_POST, 'botlink'), $endtime);
  setcookie("show_history",  safe_index($_POST, 'show_history'),  $endtime);
  setcookie("show_quality",  safe_index($_POST, 'show_quality'),  $endtime);
  header("Location: config.php");
}

showPageHeader("Customise infobot-www", $csslist);
include "theme/header.html";

showBreadcrumbs("config", "my settings");

$prop_del = showPropDel() ? "checked='1'": "";
$botlink = showBotDelLink() ? "checked='1'": "";
$show_history = showHistory() ? "checked='1'": "";
$show_quality = showQuality() ? "checked='1'": "";
$showBotLink = safe_index($_REQUEST, 'master', false);

?>
<h2>Set infobot-www configuration options</h2>
<div class='config' style='margin-top:1em;margin-bottom:2em;'>
  <form method='post'>
  <table>
    <tr>
      <td><input type='checkbox' name='prop_del' value='1'
          <?php echo $prop_del; ?> /></td>
      <td>Show "suggest deletion" controls</td>
    </tr>
    <tr>
      <td><input type='checkbox' name='show_history' value='30'
          <?php echo $show_history; ?> /></td>
      <td>Show factoid history</td>
    </tr>
    <tr>
      <td><input type='checkbox' name='show_quality' value='30'
          <?php echo $show_quality; ?> /></td>
      <td>Show factoid quality metrics</td>
    </tr>
<?php

if ($showBotLink) {
  ?>
    <tr>
      <td><input type='checkbox' name='botlink' value='1'
          <?php echo $botlink; ?> /></td>
      <td>Show bot scripting links</td>
    </tr>
  <?php
}
?>
  </table>
  <input type='submit' name='action' value='Submit' />
  </form>
</div>

<?php
include "theme/footer.html";
showPageFooter();
