<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2010 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

function showPageHeader($title, $css) {
  print '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="content-type" content="text/html;charset=utf-8" />
  <title>';
  print htmlentities($title, ENT_QUOTES, "UTF-8");
  print '
  </title>
  <link href="theme/favicon.ico" rel="shortcut icon" type="image/x-icon" />
  <link href="theme/debgit.css" rel="stylesheet" type="text/css" />';
  foreach ($css as $c) {
    printf('<link href="theme/%s" rel="stylesheet" type="text/css" />', $c);
  }
  print '
</head>
<body>
';
}

function showBreadCrumbs($type, $description) {
  echo "<div class='page_header'> ";
  list($links, $urls) = make_breadcrumbs_data($type, $description);

  $list = array();
  for ($i=0; $i<count($urls); $i++) {
    if ($urls[$i]) {
      $lists[] = sprintf("<a href='%s'>%s</a>", $urls[$i],
                              htmlentities($links[$i], ENT_QUOTES, "UTF-8"));
    } else {
      $lists[] = sprintf("%s", htmlentities($links[$i], ENT_QUOTES, "UTF-8"));
    }
  }
  echo join(" ‹ ", $lists);
  echo "</div>";
}

