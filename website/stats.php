<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2010 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

// error_reporting(E_STRICT | E_ALL);

require_once "inc/utils.php";
require_once "theme/pageheader.php";
require_once "theme/pagefooter.php";
require_once "inc/factoids.php";
require_once "inc/factoidlist.php";
require_once 'inc/nav.php';
require_once 'inc/statslist.php';
require_once 'inc/stats.php';
require_once 'inc/site-info.php';

$l = new statslist();

$l->addGroup("recent", "Recent activity");
$l->add("recently-changed", "getRecentlyChanged",
        "on recently created or modified factoids",
        "List of recently created or modified factoids");
$l->add("recently-created", "getRecentlyCreated", "on recently created factoids",
        "List of recently created factoids");
$l->add("recently-modified", "getRecentlyModified", "on recently modified factoids",
        "List of recently modified factoids");
$l->add("recently-active-creators", "getRecentlyActiveCreator",
        "on recently active creators",
        "List of recently active creators");
$l->add("recently-active-editors", "getRecentlyActiveModifier",
        "on recently active editors",
        "List of recently active editors");
$l->add("never-requested", "getNeverRequested",
        "on never-requested factoids",
        "List of never-requested factoids");
/*$l->add("not-recently-requested", "getNotRecentlyRequested",
        "on not-recently requested factoids",
        "List of not-recently requested factoids");*/
$l->add("recently-requested", "getRecentlyRequested",
        "on recently requested factoids",
        "List of recently requested factoids");

$l->addGroup("creators", "About factoid creators");
$l->add("created-by-creator", "getCreatedByOwner", "on factoids created",
        "List of factoid creators and the number of factoids created");
$l->add("aliases-by-creator", "getAliasesCreated", "on aliases created",
        "List of alias creators and the number of aliases created");
$l->add("average-lengths-by-creator", "getFactoidLengthsByOwner",
        "on average factoid lengths created",
        "Average factoid lengths by creator");
$l->add("requests-by-creator", "getRequestsByOwner", "on number of requests made",
        "Average number of factoids requested by creator");


$l->addGroup("editors", "About factoid editors");
$l->add("modified-by-modifier", "getModifiedByEditor", "on factoids modified",
        "List of factoid modifiers and the number of factoids modified");
$l->add("average-lengths-by-editor", "getFactoidLengthsByEditor",
        "on average factoid lengths modified",
        "Average factoid lengths by editor");
$l->add("requests-by-editor", "getRequestsByEditor", "on number of requests made",
        "Average number of factoids requested by editor");

$l->addGroup("contents", "About factoid contents");
$l->add("factoid-with-urls", "getURLs", "on factoids containing URLs",
        "List of factoids containing URLs");
$l->add("very-long-factoids", "getFactoidByLength", "on very long factoids",
        "List of very long factoids");

$l->addGroup("summary", "Summary");
$l->add("stats-table", "getStatsTable", "on factoids authors",
        "Statistics on factoid authors");
$l->add("overall-stats", "getOverallStats", "all factoids",
        "Overall statistics on all factoids");


$options = getOptions();
$query = safe_index($_REQUEST, 'q');

if (safe_index($_REQUEST, 'mr') && $l->exists($query)) {
  header("Content-type: text/plain");
  $l->run($query);
  exit;
}

$description = $l->getShortDesc($query);

$title = "Factoid statistics $description";
showPageHeader($title, $csslist);
include "theme/header.html";

showBreadcrumbs("stats", $description);

print "<div class='stats'>";
$l->run($query);
print "</div>";

$l->showQueryList();
showForm("", "", $options);

include "theme/footer.html";
showPageFooter();

