<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2010-2012 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

require 'conf/db.php';


try {
  $dbh = new PDO("mysql:host=$db_server;dbname=$db_name;charset=utf8",
                  $db_user, $db_password);
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
  error_log("Could not connect to db: ". $e->getMessage());
  die("Could not connect to db (see logs)");
}

# Force the connection to be utf-8 since the html we are sending is utf-8.
# this repeats the above setting of the charset because that option was silently
# ignored until PHP 5.3.6 (i.e. in squeeze).
$dbh->query("SET NAMES utf8");

function db_error($exception) {
  global $dbh;
  error_log("infobot-web sql error: [$exception] "
      . implode(" // ", $dbh->errorInfo()));
  die("Query error");
}

?>
