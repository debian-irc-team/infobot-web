<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2009 Alexander Tait Brotman
 Copyright 2010 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

require_once 'inc/utils.php';

function showForm($term, $query, $options) {
  $wild  = ($options['wild']) ? '' : 'checked="checked"';
  $clean = ($options['clean']) ? '' : 'checked="checked"';

  $q = htmlspecialchars(stripslashes($query), ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML401);
  echo "
    <form action='search.php' class='botsearchform'>
    <table>
    <tr>
      <td align='right'>Search Term:</td>
      <td><input type='text' name='q' value='$q' /></td>
      <td>('*' Can be used as a wildcard)</td>
      <td>No auto wildcards</td>
      <td><input type='checkbox' name='nowild' value='1' $wild /></td>
    </tr>
    <tr>
      <td align='right'>Field to Search:</td>
      <td><select name='term'>";

  printf("<option value='key'%s>Factoid Key</option>",
              $term =="key" ? ' selected="selected"' : '');
  printf("<option value='value'%s>Factoid Value</option>",
              $term =="value" ? ' selected="selected"' : '');
  printf("<option value='author'%s>Factoid Author</option>",
              $term =="author" ? ' selected="selected"' : '');

  echo "
        </select>
      </td>
      <td><input type='submit'/></td>
      <td>Show internal factoids</td>
      <td><input type='checkbox' name='noclean' value='1' $clean /></td>
    </tr>
    </table>
    </form>
    <hr/>";
}


function getOptions() {
  $options = array();
  $sorts = array('key', 'age', 'value');
  $options['sort'] = safe_index($_REQUEST, 'sort') && 
                     in_array($_REQUEST['sort'], $sorts) ? $_REQUEST['sort'] : "";
  $options['wild'] = ! safe_index($_REQUEST, 'nowild') ||
                      $_REQUEST['nowild'] == 'n' || $_REQUEST['nowild'] == '0';
  $options['reverse'] = safe_index($_REQUEST, 'reverse') && 
                      ($_REQUEST['reverse'] == 'y' || $_REQUEST['reverse'] == '1');
  $options['clean'] = ! safe_index($_REQUEST, 'noclean') ||
                      $_REQUEST['noclean'] == 'n' || $_REQUEST['noclean'] == '0';
  $options['days'] = max((int) safe_index($_REQUEST, 'days', 0), 0);
  return $options;
}
