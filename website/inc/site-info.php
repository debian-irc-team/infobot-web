<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2010-2021 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

$csslist = array('infobot-web.css');

function showWelcomeMessage() {
  echo "
  <p>In #debian, the bot <code>dpkg</code> provides answers to a large number of frequently
  asked questions. You can find <code>dpkg</code> on both
  <a href='http://oftc.net/'>OFTC</a> and
  <a href='http://libera.chat/'>Libera.chat</a>.</p>

  <p>Further information about <code>dpkg</code> can be found on the
  <a href='http://wiki.debian.org/IRC/DpkgBot'>Debian wiki</a>.</p>

  <p>The code for <code>infobot-web</code> that powers this site can be browsed via
  <a href='https://salsa.debian.org/debian-irc-team/infobot-web'>salsa.debian.org</a> or downloaded directly with
  <code>git clone https://salsa.debian.org/debian-irc-team/infobot-web.git</code>. </p>

  ";
}

function make_breadcrumbs_data($type, $description) {
  $links = array("IRC bots");
  $urls  = array("/");

  switch ($type) {
    case "search":
      $urls[] = "search.php";
      $links[] = "Factoid search";
      if ($description) {
        $urls[] = "";
        $links[] = "Searching by factoid $description";
      }
      break;
    case "stats":
      $urls[] = "stats.php";
      $links[] = "Factoid statistics";
      if ($description) {
        $urls[] = "";
        $links[] = "Stats $description";
      }
      break;
    case "scripts":
      $urls[] = "/scripts";
      $links[] = "Support scripts";
      if ($description) {
        $urls[] = "";
        $links[] = "Script $description";
      }
      break;
    default:
      break;
  }

  return array($links, $urls);
}
