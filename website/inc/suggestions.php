<?php
/*
 Copyright 2012 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

require_once 'db.php';


// CREATE TABLE factoids_suggestions (
//   factoid_key VARCHAR(64) NOT NULL default '',
//   suggested_by VARCHAR(80) DEFAULT NULL,
//   suggested_time INT(11) DEFAULT NULL,
//   suggested_action VARCHAR(16) default NULL,
//   suggested_value TEXT NOT NULL,
//   INDEX (factoid_key)
// );


class SuggestionType {
  const PROP_DEL = 'PROP_DEL';
  const PROP_CHANGE = 'PROP_CHANGE';
}

class Suggestion {

  var $_factoid_key;
  var $_suggested_by;
  var $_suggested_time;
  var $_suggested_action;
  var $_suggested_value;

  function load($row) {
    $this->_factoid_key = $row["factoid_key"];
    $this->_suggested_by = $row["suggested_by"];
    $this->_suggested_time = $row["suggested_time"];
    $this->_suggested_action = $row["suggested_action"];
    $this->_suggested_value = $row["suggested_value"];
  }

  function propose_deletion($key) {
    $this->_factoid_key = $key;
    $this->_suggested_action = SuggestionType::PROP_DEL;
    $this->_set_metadata();
  }

  function propose_change($key, $value) {
    $this->_factoid_key =$key;
    $this->_suggested_action = SuggestionType::PROP_CHANGE;
    $this->_suggested_value = $value;
    $this->_set_metadata();
  }

  function _set_metadata() {
    $this->_suggested_by = $_SERVER['REMOTE_ADDR'];
    $this->_suggested_time = time();
  }

  function _formatShortDate($date) {
    return date("Y-m-d", $date);
  }

}


class _Suggestions {

  var $dbh;

  function __construct($db_handle) {
    $this->dbh = $db_handle;
  }

  function count($type) {
    $sql = "SELECT COUNT(*)
              FROM factoids_suggestions
              WHERE suggested_action = :type";
    $sh = $this->dbh->prepare($sql);
    $sh->bindParam(':type', $type);
    $sh->execute();
    $row = $sh->fetch();
    return $row[0];
  }

  function suggestionsForFactoid($key='%', $unique=true) {
    // MySQL uses a case-insensitive LIKE if the collation permits.
    $sql = "SELECT * FROM factoids_suggestions
              WHERE factoid_key LIKE :key";
    if ($unique) {
      $sql .= " GROUP BY factoid_key";
    }
    $sh = $this->dbh->prepare($sql);
    $sh->bindParam(':key', $key);
    $sh->execute();

    $list = $this->suggestionListFromResult($sh);
    return $list;
  }

  function createSuggestionForFactoid($suggestion) {
    // MySQL uses a case-insensitive LIKE if the collation permits.
    $sql = "INSERT INTO factoids_suggestions
              (factoid_key, suggested_by, suggested_time,
                suggested_action, suggested_value)
            VALUES (:key, :suggested_by, :suggested_time,
                  :suggested_action, :suggested_value)";
    $sh = $this->dbh->prepare($sql);
    $sh->bindParam(':key', $suggestion->_factoid_key);
    $sh->bindParam(':suggested_by', $suggestion->_suggested_by);
    $sh->bindParam(':suggested_time', $suggestion->_suggested_time);
    $sh->bindParam(':suggested_action', $suggestion->_suggested_action);
    $sh->bindParam(':suggested_value', $suggestion->_suggested_value);
    $sh->execute();
  }

  function suggestionListFromResult($sh) {
    // take a PDO statement handle and create a suggestion list from it
    $retval = array();
    while ($row = $sh->fetch(PDO::FETCH_ASSOC)) {
      $suggestion = new Suggestion;
      $suggestion->load($row);

      array_push($retval, $suggestion);
    }
    $sh->closeCursor();
    return $retval;
  }
}

$suggestions = new _Suggestions($dbh);

?>
