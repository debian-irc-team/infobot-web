<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2009 Alexander Tait Brotman
 Copyright 2010 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

require_once "inc/factoids.php";
require_once "inc/utils.php";

function showFactoidList($results, $options) {
  $format = safe_index($options, 'format', 'html');
  switch ($format) {
    case 'text':
      showFactoidListAsText($results);
      break;
    default:
      showFactoidListAsHtml($results, $options);
  }
}

function showFactoidListAsHtml($results, $options) {
  $showQuality = safe_index($options, 'showQuality', false);
  $showPropDel = safe_index($options, 'showPropDel', false);
  $extracol = '';
  if ($showQuality) {
    $extracol .= "<th width='10%'>Flags</th>";
  }
  if ($showPropDel) {
    print "<form method='post' action='suggest.php'>
          <input type='hidden' name='action' value='prop_del' />
    ";
    $extracol .= "<th width='5%'>Del?</th>";
  }
  echo "<table width='100%' class='searchresults'
      style='table-layout:fixed; overflow-wrap:break-word;'>
    <tr><th width='15%'>Factoid&nbsp;Key</th><th>Factoid&nbsp;Value</th>$extracol</tr>";

  foreach( $results as $factoid )
  {
    $key = $factoid->_factoid_key;
    $extracol = '';
    //$mod_time = ($factoid->_modified_time|$factoid->_created_time);
    if ($factoid->_modified_time != "" || $factoid->_history_time > 0){
      $mod_time = "M: ".$factoid->getShortModifiedOrHistoryDate();
    } else {
      $mod_time = "C: ".$factoid->getShortCreatedDate();
    }
    $ukey = urlencode($key);
    $hkey = htmlentities($key, ENT_QUOTES, "UTF-8");
    $hmodtime = htmlentities($mod_time, ENT_QUOTES, "UTF-8");
    $hval = $factoid->getFactoidValue();
    if ($showQuality) {
      $quality = new FactoidQuality($factoid);
      $extracol .= sprintf("<td>%s</td>", $quality->short());
    }
    if ($showPropDel)
      $extracol .= sprintf("<td><input type='checkbox' name='del[]' value='%s' /></td>", $hkey);
    echo "<tr>
          <td>
            <a href='factoid.php?key=$ukey'>$hkey</a><br />
            <span class='mtime'>$hmodtime</span>
          </td>
          <td>$hval</td>$extracol
        </tr>";
  }
  echo "</table><br />";
  if ($showPropDel) {
    echo "<input type='submit' name='submit' value='Suggest deletions' />
          </form><br /><br />\n";
  }
}

function showFactoidListAsText($results) {
  foreach( $results as $factoid )
  {
    $key = $factoid->_factoid_key;
    $value = $factoid->_factoid_value;
    echo "$key\t$value\n";
  }
}

function showFactoidHistoryList($results) {
  echo "<table width='100%' class='searchresults'><tr>
      <th>End&nbsp;Date</th>
      <th>Factoid&nbsp;Value</th>
      </tr>";

  foreach( $results as $factoid )
  {
    $mod_time = $factoid->getShortHistoryDate();
    $hmodtime = htmlentities($mod_time, ENT_QUOTES, "UTF-8");
    $hval = $factoid->getFactoidValue();
    echo "<tr>
          <td>$hmodtime</td>
          <td>$hval</td>
        </tr>";
  }
  echo "</table><br />";
}
