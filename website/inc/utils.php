<?php
/*
 Copyright 2010 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

function safe_index($array, $index, $default="") {
  return array_key_exists($index, $array) ? $array[$index] : $default;
}

// $_REQUEST contains $_COOKIE... except when it doesn't
// https://bugs.php.net/bug.php?id=61893
function _find_config_option($key, $default=false) {
  $v = safe_index($_REQUEST, $key, $default);
  if ($v !== false) {
    return $v;
  }
  return safe_index($_COOKIE, $key, $default);
}

function showPropDel() {
  return _find_config_option('prop_del');
}

function showBotDelLink() {
  return _find_config_option('botlink');
}

function showHistory() {
  return _find_config_option('show_history');
}

function showQuality() {
  return _find_config_option('show_quality');
}

function autoDisplayOptions() {
  return array(
    'showPropDel'    => showPropDel(),
    'showQuality'    => showQuality(),
    'showBotDelLink' => showBotDelLink(),
    'showHistory'    => showHistory(),
    'format'         => 'html'
  );
}