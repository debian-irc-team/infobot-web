<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2009 Heikki Hokkanen <hoxu@fealdia.org>
 Copyright 2010-2012 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

require_once "inc/utils.php";

function getCreatedByOwner() {
  # number of factoids created by owner
  getCreatedBy("created_by", "creator");
}

function getModifiedByEditor() {
  # number of factoids modified by editor
  getCreatedBy("modified_by", "editor");
}

function getCreatedBy($field, $caption) {
  global $dbh;
  # number of factoids created by owner/editor
  $minmade = 20;
  $sql = "SELECT IF(INSTR($field,'!'),
                    SUBSTR($field,1,INSTR($field,'!') - 1),
                    $field) owner,
                COUNT(*) AS factoids
          FROM factoids
          WHERE $field IS NOT NULL
          GROUP BY owner
          HAVING factoids > $minmade
          ORDER BY factoids DESC";
  print "<h1>Factoid {$caption}s with more than $minmade factoids</h1>";
  print "<table>";
  print "<tr><th>Person</th><th>Number</th></tr>\n";
  foreach($dbh->query($sql, PDO::FETCH_ASSOC) as $row) {
    printf("<tr><td><a href='search.php?author=%s'>%s</a></td><td>%d</td></tr>\n",
                urlencode($row['owner']),
                htmlentities($row['owner'], ENT_QUOTES, "UTF-8"),
                $row['factoids']);
  }
  print "</table>";
  print "<p>(restricted to {$caption}s with more than $minmade factoids)</p>";
}

function getAliasesCreated() {
  global $dbh;
  # number of redirect factoids by owner
  $sql = "SELECT IF(INSTR(created_by,'!'),
                   SUBSTR(created_by,1,INSTR(created_by,'!') - 1),
                   created_by) owner,
                COUNT(*) AS factoids
          FROM factoids
          WHERE created_by IS NOT NULL
              AND factoid_value RLIKE '^<reply> *see .*'
          GROUP BY owner
          ORDER BY factoids DESC";
  print "<h1>Factoid aliases creators</h1>";
  print "<table>";
  print "<tr><th>Person</th><th>Number</th></tr>\n";
  foreach($dbh->query($sql, PDO::FETCH_ASSOC) as $row) {
    printf("<tr><td><a href='search.php?author=%s'>%s</a></td><td>%d</td></tr>\n",
                urlencode($row['owner']),
                htmlentities($row['owner'], ENT_QUOTES, "UTF-8"),
                $row['factoids']);
  }
  print "</table>";
}

function getFactoidLengthsByOwner() {
  # average length of factoid by owner
  getFactoidLengthsBy("created_by", "creator");
}

function getFactoidLengthsByEditor() {
  # average length of factoid by editor
  getFactoidLengthsBy("modified_by", "editor");
}

function getFactoidLengthsBy($field, $caption) {
  global $dbh;
  # average length of factoid by owner
  $sql = "SELECT IF(INSTR($field,'!'),
                   SUBSTR($field,1,INSTR($field,'!') - 1),
                   $field) owner,
                AVG(LENGTH(factoid_value)) AS len,
                SUM(LENGTH(factoid_value)) AS total,
                COUNT(*) AS factoids
          FROM factoids
          WHERE $field IS NOT NULL
              AND factoid_value NOT RLIKE '^<reply> *see .*'
          GROUP BY owner
          HAVING factoids > 20
          ORDER BY len DESC";
  print "<h1>Factoid length by $caption</h1>";
  print "<table>";
  print "<tr><th>Person</th><th>Average factoid length</th>
              <th>Total factoids</th><th>Total length</th></tr>\n";
  foreach($dbh->query($sql, PDO::FETCH_ASSOC) as $row) {
    printf("<tr><td><a href='search.php?author=%s'>%s</a></td>",
              urlencode($row['owner']),
              htmlentities($row['owner'], ENT_QUOTES, "UTF-8"));
    printf("<td>%3.1f</td><td>%d</td><td>%d</td></tr>\n",
              $row['len'], $row['factoids'], $row['total']);
  }
  print "</table>";
  print "<p>(restricted to {$caption}s with more than 20 factoids; aliases excluded)</p>";
}

function getRequestsByOwner() {
  # average number of requests by owner
  getRequestsBy("created_by", "owner");
}

function getRequestsByEditor() {
  # average number of requests by editor
  getRequestsBy("modified_by", "editor");
}

function getRequestsBy($field, $caption) {
  global $dbh;
  # average number of requests by owner/editor
  # minimum number of requests to be included in analysis
  $minrequests = 50;
  $sql = "SELECT IF(INSTR($field,'!'),
                   SUBSTR($field,1,INSTR($field,'!') - 1),
                   $field) owner,
                AVG(requested_count) AS requests,
                COUNT(factoid_key) AS total_factoids,
                SUM(requested_count) AS total_requests
          FROM factoids
          WHERE $field IS NOT NULL
            AND requested_count IS NOT NULL
          GROUP BY owner
          HAVING requests > :minrequests
          ORDER BY requests DESC";
  $sh = $dbh->prepare($sql);
  $sh->bindParam(':minrequests', $minrequests);
  $sh->execute();

  print "<h1>Factoid requests by $caption</h1>";
  print "<table>";
  print "<tr><th>Person</th><th>Average requests</th>
            <th>Total factoids</th><th>Total requests</th></tr>\n";
  while ($row = $sh->fetch(PDO::FETCH_ASSOC)) {
    printf("<tr><td><a href='search.php?author=%s'>%s</a></td>",
                urlencode($row['owner']),
                htmlentities($row['owner'], ENT_QUOTES, "UTF-8"));
    printf("<td>%3.1f</td><td>%d</td><td>%d</td></tr>",
            $row['requests'], $row['total_factoids'], $row['total_requests']);
  }
  print "</table>";
  print "<p>(restricted to {$caption}s with an average of more than $minrequests requests)</p>";
}

function getStatsTable() {
  global $dbh;
  # author stats table
  $field = 'created_by';
  $minfactoids = 20;
  $sql = "SELECT IF(INSTR($field,'!'),
                   SUBSTR($field,1,INSTR($field,'!') - 1),
                   $field) author,
                COUNT(*) AS factoids,
                SUM(requested_count) AS requests,
                AVG(requested_count) AS requests_per_factoids,
                FROM_UNIXTIME(MIN(created_time)) AS first_factoid,
                FROM_UNIXTIME(MAX(created_time)) AS last_factoid
          FROM factoids
          WHERE $field IS NOT NULL
          GROUP BY author
          HAVING factoids > $minfactoids
          ORDER BY factoids DESC";
  print "<h1>Factoid creator statistics</h1>";
  print "<table>";
  print "<tr><th>Person</th>
            <th>Number of factoids</th>
            <th>Total requests</th>
            <th>Average requests</th>
            <th>First created</th>
            <th>Last created</th></tr>\n";
  foreach($dbh->query($sql, PDO::FETCH_ASSOC) as $row) {
    print "<tr><td><a href='search.php?author=".urlencode($row['author'])."'>".htmlentities($row['author'], ENT_QUOTES, "UTF-8")."</a></td>"
        ."<td>$row[factoids]</td>"
        ."<td>".sprintf("%d", $row['requests'])."</td>"
        ."<td>".sprintf("%3.1f", $row['requests_per_factoids'])."</td>"
        ."<td>$row[first_factoid]</td>"
        ."<td>$row[last_factoid]</td>"
        ."</tr>\n";
  }
  print "</table>";
  print "<p>(restricted to creators with more than 20 factoids</p>";
}


function getOverallStats() {
  global $dbh;
  # overall stats
  $sql = "SELECT COUNT(*) AS factoids,
                SUM(requested_count) AS requests,
                AVG(requested_count) AS requests_per_factoids,
                AVG(LENGTH(factoid_key)) AS avg_key_length,
                AVG(LENGTH(factoid_value)) AS avg_value_length,
                FROM_UNIXTIME(MIN(created_time)) AS first_factoid,
                FROM_UNIXTIME(MAX(created_time)) AS last_factoid
          FROM factoids";
  $sh = $dbh->query($sql);
  $row = $sh->fetch(PDO::FETCH_ASSOC);
  print "<h1>Overall factoid statistics</h1>";
  print "<table>";
  print "<tr><td>Total factoids</td><td>$row[factoids]</td>\n";
  print "<tr><td>Total requests</td><td>".sprintf("%d", $row['requests']) ."</td>\n";
  print "<tr><td>Average requests per factoid</td><td>".sprintf("%3.1f", $row['requests_per_factoids']) ."</td>\n";
  print "<tr><td>Average factoid key length</td><td>".sprintf("%3.1f",$row['avg_key_length']) ."</td>\n";
  print "<tr><td>Average factoid length</td><td>".sprintf("%3.1f",$row['avg_value_length']) ."</td>\n";
  print "<tr><td>First factoid</td><td>$row[first_factoid]</td>\n";
  print "<tr><td>Last factoid</td><td>$row[last_factoid]</td>\n";
  print "</table>";
}


function getRecentlyCreated() {
  # factoids created in the last $days days
  getRecently("created_time", "created", 14);
}

function getRecentlyModified() {
  # factoids created in the last $days days
  getRecently("modified_time", "modified", 14);
}

function getRecentlyChanged() {
  # factoids created or modified in the last $days days
  getRecently("changed_time", "created or modified", 14);
}

function getRecently($field, $caption, $days) {
  global $dbh;
  global $factoids;
  # factoids created in the last $days days
  $maxdays = 28;
  $days = min((int)safe_index($_REQUEST, 'days', $days), $maxdays);
  $seconds = floor($days*86400);

  if ($field == "changed_time") {
    $sql = "
          SELECT factoids.*,
                factoids_history.history_time,
                GREATEST(
                    IFNULL(factoids.created_time,0),
                    IFNULL(factoids.modified_time,0),
                    IFNULL(factoids_history.history_time,0)
                  ) AS changed_time
          FROM factoids
          LEFT JOIN factoids_history
            ON factoids.factoid_key = factoids_history.factoid_key
          WHERE (factoids.created_time > UNIX_TIMESTAMP(NOW())-$seconds
            OR factoids.modified_time > UNIX_TIMESTAMP(NOW())-$seconds
            OR factoids_history.history_time > UNIX_TIMESTAMP(NOW())-$seconds)"
            .$factoids->cleanClause(true, 'factoids.')."
          GROUP BY factoids.factoid_key
          ORDER BY changed_time DESC";
  } else {
    $sql = "SELECT *
            FROM factoids
            WHERE $field > UNIX_TIMESTAMP(NOW())-$seconds "
            .$factoids->cleanClause(true)."
            ORDER BY $field DESC";
  }
  $sh = $dbh->query($sql);
  $result = $factoids->factoidListFromResult($sh);
  print "<h1>Recently $caption factoids (last $days days)</h1>";
  showFactoidList($result, autoDisplayOptions());
}


function getRecentlyActiveCreator() {
  # factoids created in the last $days days
  getRecentlyActive("created_by", "created_time", "creators", 90);
}

function getRecentlyActiveModifier() {
  # factoids created in the last $days days
  getRecentlyActive("modified_by", "modified_time", "editors", 90);
}


function getRecentlyActive($field, $timefield, $caption, $days) {
  global $dbh;
  # list of owners/editors who have been active in last $days days
  $maxdays = 180;
  $days = min((int)safe_index($_REQUEST, 'days', $days), $maxdays);
  $seconds = floor($days*86400);

  $sql = "SELECT IF(INSTR($field,'!'),
                    SUBSTR($field,1,INSTR($field,'!') - 1),
                    $field) owner,
                COUNT(*) AS factoids
          FROM factoids
          WHERE $field IS NOT NULL
            AND $timefield > UNIX_TIMESTAMP(NOW())-$seconds
          GROUP BY owner
          ORDER BY factoids DESC";
  print "<h1>Recently active factoid $caption (last $days days)</h1>";
  print "<table>";
  print "<tr><th>Person</th><th>Number</th></tr>\n";
  foreach ($dbh->query($sql, PDO::FETCH_ASSOC) as $row) {
    printf("<tr><td><a href='search.php?author=%s'>%s</a></td><td>%d</td></tr>\n",
                urlencode($row['owner']),
                htmlentities($row['owner'], ENT_QUOTES, "UTF-8"),
                $row['factoids']);
  }
  print "</table>";
}

function getURLs() {
  global $dbh;
  global $factoids;
  # List of factoids containing http://, https:// and ftp:// URLs
  $sql = "SELECT *
          FROM factoids
          WHERE factoid_value RLIKE '(https?|ftp)://'";
  $sh = $dbh->query($sql);
  $result = $factoids->factoidListFromResult($sh);
  print "<h1>Factoids containing URLs (".count($result)." results)</h1>";
  showFactoidList($result, autoDisplayOptions());
}

function getFactoidByLength() {
  global $dbh;
  global $factoids;
  $minlength = 300;
  # average length of factoid by owner
  $sql = "SELECT *, LENGTH(factoid_value) AS len
          FROM factoids
          WHERE 1 "
            .$factoids->cleanClause(true, 'factoids.')."
          HAVING len > :minlength
          ORDER BY len DESC";
  $sh = $dbh->prepare($sql);
  $sh->bindParam(':minlength', $minlength);
  $sh->execute();
  $result = $factoids->factoidListFromResult($sh);
  print "<h1>Long factoids (".count($result)." results)</h1>";
  showFactoidList($result, autoDisplayOptions());
  print "<p>(restricted to factoids with more than $minlength bytes)</p>";
}

function getNeverRequested() {
  global $dbh;
  global $factoids;
  # List of factoids that were only requested a few times
  $maxrequests = 50;
  $requests = min((int)safe_index($_REQUEST, 'requests', 0), $maxrequests);
  $sql = "SELECT *
          FROM factoids
          WHERE requested_count <= :requests"
          . $factoids->cleanClause(true);
  $sh = $dbh->prepare($sql);
  $sh->bindParam(':requests', $requests);
  $sh->execute();
  $result = $factoids->factoidListFromResult($sh);
  print "<h1>Factoids requested $requests". ($requests ? " or fewer" : "")." times (".count($result)." results)</h1>";
  showFactoidList($result, autoDisplayOptions());
}

function getNotRecentlyRequested() {
  global $dbh;
  global $factoids;
  # List of factoids that were not requested recently
  $mindays = 365;
  $days = max((int)safe_index($_REQUEST, 'days', 730), $mindays);
  $seconds = floor($days*86400);

  $sql = "SELECT *
          FROM factoids
          WHERE requested_time < UNIX_TIMESTAMP(NOW())-:seconds"
          . $factoids->cleanClause(true);
  $sh = $dbh->prepare($sql);
  $sh->bindParam(':seconds', $seconds);
  $sh->execute();
  $result = $factoids->factoidListFromResult($sh);
  print "<h1>Factoids not requested in last $days days (".count($result)." results)</h1>";
  showFactoidList($result, autoDisplayOptions());
}


function getRecentlyRequested() {
  global $dbh;
  global $factoids;
  # List of factoids that were requested recently, sorted by number of requests
  $maxdays = 90;
  $days = min((int)safe_index($_REQUEST, 'days', 30), $maxdays);
  $maxfactoids = (int)safe_index($_REQUEST, 'limit', 10000);
  $minrequests = (int)safe_index($_REQUEST, 'minrequests', 1);
  $seconds = floor($days*86400);

  $sql = "SELECT *
          FROM factoids
          WHERE requested_time > UNIX_TIMESTAMP(NOW())-:seconds
            AND requested_count >= :minrequests "
          . $factoids->cleanClause(true)
          ." ORDER BY requested_count DESC
          LIMIT $maxfactoids";

  $sh = $dbh->prepare($sql);
  $sh->bindParam(':seconds', $seconds);
  $sh->bindParam(':minrequests', $minrequests);
  $sh->execute();
  $result = $factoids->factoidListFromResult($sh);
  if (safe_index($_REQUEST, 'mr')) {
    showFactoidList($result, array('format' => 'text'));
  } else {
    print "<h1>Factoids requested in last $days days (".count($result)." results)</h1>";
    showFactoidList($result, autoDisplayOptions());
    print "Factoid list sorted by number of times the factoid was ever requested.";
  }
}

