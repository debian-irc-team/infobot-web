<?php
/*
 Copyright 2010-2012 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

class stattype {

  var $code;
  var $func;
  var $shortdesc;
  var $longdesc;
  var $grouping;

  function __construct($code, $function, $shortdesc, $longdesc, $grouping="") {
    $this->code = $code;
    $this->func = $function;
    $this->shortdesc = $shortdesc;
    $this->longdesc = $longdesc;
    $this->grouping = $grouping;
  }
}

class statslist {
  var $list   = array();
  var $groups = array();
  var $lastgroup = "";

  function addGroup($code, $name) {
    if (! array_key_exists($code, $this->groups)) {
      $this->groups[$code] = array();
    }
    $this->groups[$code]['_group-name'] = $name;
    $this->lastgroup = $code;
  }

  function addstat($stat) {
    $this->list[$stat->code] = $stat;
    if (! array_key_exists($stat->grouping, $this->groups)) {
      $this->groups[$stat->grouping] = array();
    }
    $this->groups[$stat->grouping][] = $stat;
  }

  function add($code, $function, $shortdesc, $longdesc, $grouping="") {
    if ($grouping === "" && $this->lastgroup) {
      $grouping = $this->lastgroup;
    }
    $this->addstat(new stattype($code, $function, $shortdesc, $longdesc, $grouping));
  }

  function run($code) {
    if (array_key_exists($code, $this->list) && $this->list[$code]->func) {
      try {
        call_user_func($this->list[$code]->func);
      } catch (PDOException $e) {
        db_error($e);
        die("Error collating statistics");
      }
    }
  }

  function exists($code) {
    return (array_key_exists($code, $this->list) && $this->list[$code]->func);
  }

  function getShortDesc($code) {
    if (array_key_exists($code, $this->list)) {
      return $this->list[$code]->shortdesc;
    }
  }

  function showQueryList() {
    print "<h1>Available searches</h1>";
    foreach ($this->groups as $group => $stats) {
      print "<h2>".$stats['_group-name']."</h2>\n";
      print "<ul class='stattypes'>\n";
      foreach ($stats as $code => $stat) {
        if ($code !== '_group-name') {
          print "<li><a href='?q={$stat->code}'>{$stat->longdesc}</a></li>\n";
        }
      }
      print "</ul>\n";
    }
  }

}

