<?php
/*
 Copyright 2005 Michael O'Connor <stew@vireo.org>
 Copyright 2010-2012 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

require_once 'db.php';
require_once 'conf/display.php';

class _factoids
{
    var $min_length = 2;
    var $dbh;

    function __construct($db_handle) {
        $this->dbh = $db_handle;
    }

    function getFactoidByKey( $key )
    {
        // MySQL uses a case-insensitive LIKE if the collation permits.
        // The default charset for MySQL is latin1 with the latin1_swedish_ci
        // collation and this is (incorrectly) usually unchanged.
        $sh = $this->dbh->prepare("SELECT * FROM factoids
                                    WHERE factoid_key LIKE :key");
        $sh->bindParam(':key', $key);
        $sh->execute();
        $list = $this->factoidListFromResult($sh);
        return array_shift($list);
    }

    function getFactoidsByKeyList( $keys )
    {
        $list = array();
        $sh = $this->dbh->prepare("SELECT * FROM factoids
                                    WHERE factoid_key = :key");
        foreach($keys as $k) {
            $sh->bindParam(':key', $k);
            $sh->execute();
            array_push($list, new _factoid($sh->fetch(PDO::FETCH_ASSOC)));
        }
        return $list;
    }

    function getFactoidsByKey( $pattern, $options )
    {
        if ($options['wild'] && substr($pattern, 1, 1) != "*") $pattern = "*$pattern";
        if ($options['wild'] && substr($pattern, -1, 1) != "*") $pattern = "$pattern*";
        $pattern = str_replace( "*", "%", $pattern );

        if (!$this->checkPattern($pattern)) return;

        $sql = "SELECT * FROM factoids WHERE factoid_key LIKE :key";
        $sql .= $this->cleanClause($options['clean']);
        $sql .= $this->_sortClause($options['sort'], $options['reverse']);
        $sh = $this->dbh->prepare($sql);
        $sh->bindParam(':key', $pattern);
        $sh->execute();
        return $this->factoidListFromResult($sh);
    }

    function getFactoidHistory( $key, $history_length )
    {
        $days = (int) $history_length;
        $days = ($days < 1) ? 1 : $days;
        $time = time() - 24*60*60*$history_length;
        # use LIKE for case insensitive lookup
        $sql = "SELECT * FROM factoids_history WHERE factoid_key LIKE :key"
                ." AND history_time > :time ORDER BY history_time DESC";
        $sh = $this->dbh->prepare($sql);
        $sh->bindParam(':key', $key);
        $sh->bindParam(':time', $time);
        $sh->execute();
        return $this->factoidListFromResult($sh);
    }

    function getFactoidsByValue( $pattern, $options )
    {
        if ($options['wild'] && substr($pattern, 1, 1) != "*") $pattern = "*$pattern";
        if ($options['wild'] && substr($pattern, -1, 1) != "*") $pattern = "$pattern*";
        $pattern = str_replace( "*", "%", $pattern );

        if (!$this->checkPattern($pattern)) return;

        $sql = "SELECT * FROM factoids WHERE factoid_value LIKE :pattern";
        $sql .= $this->cleanClause($options['clean']);
        $sql .= $this->_sortClause($options['sort'], $options['reverse']);
        $sh = $this->dbh->prepare($sql);
        $sh->bindParam(':pattern', $pattern);
        $sh->execute();
        return $this->factoidListFromResult($sh);
    }

    function getFactoidsByRegex( $pattern, $options )
    {
        // $pattern should already be a properly escaped regex

        if (!$this->checkPattern($pattern)) return;

        $sql = "SELECT * FROM factoids WHERE factoid_value RLIKE :pattern";
        $sql .= $this->cleanClause($options['clean']);
        $sql .= $this->_sortClause($options['sort'], $options['reverse']);
        $sh = $this->dbh->prepare($sql);
        $sh->bindParam(':pattern', $pattern);
        $sh->execute();
        return $this->factoidListFromResult($sh);
    }

    function getFactoidsByAuthor( $author, $options )
    {
        if( $author == 'unknown' )
        {
            $sql = "SELECT * FROM factoids WHERE created_by IS NULL";
        }
        else
        {
            if ($options['wild']) {
              if (strpos("!", $author) === false) {
                $author .= "!";
              }
              $author .= "%";
            }

            if (!$this->checkPattern($author)) return;

            $sql = "SELECT *
                    FROM factoids
                    WHERE (created_by LIKE :pattern
                            OR modified_by LIKE :pattern2)";
            if ($options['days']) {
              $seconds = floor($options['days']*86400);
              $sql .= "
                      AND (created_time > UNIX_TIMESTAMP(NOW())-$seconds
                            OR modified_time > UNIX_TIMESTAMP(NOW())-$seconds)";
            }
            $sql .= $this->cleanClause($options['clean']);
            $sql .= $this->_sortClause($options['sort'], $options['reverse']);
        }
        $sh = $this->dbh->prepare($sql);
        $sh->bindParam(':pattern', $author);
        $sh->bindParam(':pattern2', $author);
        $sh->execute();
        return $this->factoidListFromResult($sh);
    }

    function _sortclause($sort, $reverse) {
      $s = "";
      $c = "";
      switch ($sort) {
        case 'age':
          $c = 'modified_time';
          break;
        case 'value':
          $c = 'factoid_value';
          break;
        case 'key':
          $c = 'factoid_key';
          break;
      }
      if ($c) {
        $s = " ORDER BY $c";
        if ($reverse) {
          $s .= " DESC";
        }
      }
      return $s;
    }

    static function cleanClause($clean, $prefix='') {
      if (! $clean) return "";
      return " AND {$prefix}factoid_key NOT LIKE '_default %'
               AND {$prefix}factoid_key NOT RLIKE '^#[-a-z.]+ .*'
               AND {$prefix}factoid_key NOT LIKE '% #DEL#'
               AND {$prefix}factoid_key NOT LIKE '% #del#'
               AND {$prefix}factoid_value != '#del#'";
    }

    function checkPattern($pattern) {
      return (strlen(preg_replace("/[^[:alpha:][:digit:]]/i", "", $pattern)) >= $this->min_length);
    }

    function factoidListFromResult($sh) {
      // take a PDO statement handle and create a factoid list from it
      $retval = array();
      while ($row = $sh->fetch(PDO::FETCH_ASSOC) ) {
          $factoid = new _factoid($row);
          array_push( $retval, $factoid );
      }
      $sh->closeCursor();
      return $retval;
    }
}

$factoids = new _factoids($dbh);

// CREATE TABLE `factoids` (
//   `factoid_key` VARCHAR(64) NOT NULL default '',
//   `requested_by` VARCHAR(80) default NULL,
//   `requested_time` INT(11) DEFAULT NULL,
//   `requested_count` SMALLINT(5) UNSIGNED DEFAULT NULL,
//   `created_by` VARCHAR(80) DEFAULT NULL,
//   `created_time` INT(11) DEFAULT '0',
//   `modified_by` VARCHAR(80) DEFAULT NULL,
//   `modified_time` INT(11) DEFAULT NULL,
//   `locked_by` VARCHAR(80) DEFAULT NULL,
//   `locked_time` INT(11) DEFAULT NULL,
//   `factoid_value` TEXT NOT NULL,
//   PRIMARY KEY  (`factoid_key`)
// ) TYPE=MyISAM;

class _factoid
{
    var $_factoid_key;
    var $_requested_by;
    var $_requested_time;
    var $_requested_count;
    var $_created_by;
    var $_created_time;
    var $_modified_by;
    var $_modified_time;
    var $_locked_by;
    var $_locked_time;
    var $_factoid_value;
    var $_history_time = -1;
    var $_xref_count = 0;
    var $_broken_xref_count = 0;
    var $_bug_ref_count = 0;
    var $_is_alias = false;

    function __construct($row=null) {
      if (is_array($row)) {
        $this->load($row);
      }
    }

    function load( $row )
    {
        $this->_factoid_key = $row[ "factoid_key" ];
        $this->_requested_by = $row[ "requested_by"];
        $this->_requested_time = $row[ "requested_time"];
        $this->_requested_count = $row[ "requested_count"];
        $this->_created_by = $row[ "created_by"];
        $this->_created_time = $row[ "created_time"];
        $this->_modified_by = $row[ "modified_by"];
        $this->_modified_time = $row[ "modified_time"];
        $this->_locked_by = $row[ "locked_by"];
        $this->_locked_time = $row[ "locked_time"];
        $this->_factoid_value = $row[ "factoid_value"];
        if (array_key_exists("history_time", $row)) {
            $this->_history_time = $row["history_time"];
        }
    }

    function getFactoidValue($fancy=true)
    {
        $retval = $this->_factoid_value;

        if (! $fancy) return $retval;

        if( $retval )
        {
            # start by sanitising the text
            $retval = htmlentities($retval, ENT_NOQUOTES, "UTF-8");
            if ($fancy) {
              # replace instances of <foo> with a link to factoid foo
              # (also cleans old-style <REPLY>)
              $retval = preg_replace_callback('/&lt;([^\s=].*?)(?<!-)&gt;/i',
                          array($this, 'xrefCleaner'), $retval);
              # make aliases a link to the target (<reply> see foo)
              $retval = preg_replace_callback('/^(&lt;reply&gt;\s*see\s+)(.{1,30})$/',
                          array($this, 'aliasCleaner'), $retval);
              # make http: and https: URLs a link to the target
              #print "___{$retval}___";
              $retval = preg_replace('@(https?://[a-zA-Z0-9._/~?%#&:;=+-]*?)(\.$|[.;,]\s|[)>][.;, ]|&gt;<| |$)@',
                          '<a href="\1">\1</a>\2', $retval);
              # make #bugnumber a link to the configured bug tracker
              $retval = preg_replace_callback('/#(\d{6})/',
                          array($this, 'bugLinkCleaner'), $retval);
            }
        }
        else
        {
            $retval = "&nbsp;";
        }

        return $retval;
    }

    function getFactoidLength($chars=false) {
        if ($chars) {  # return length in characters (assuming UTF-8)
            return mb_strlen($this->_factoid_value, "UTF-8");
        } else {       # return length in bytes
            return strlen($this->_factoid_value);
        }
    }

    function _webifyAuthorString( $author )
    {
        if( $author )
        {
            $shortName = preg_replace( "/([^!]*)!.*/", "$1", $author );
            $longName = preg_replace( "/[^!]*!(.*)/", "$1", $author );
            $href = "search.php?author=" . urlencode($shortName);
            $displayName = htmlentities("$shortName <$longName>", ENT_QUOTES, "UTF-8");

            $retval = "<a href='$href'>$displayName</a>";
        }
        else
        {
//            $retval = "<a href='search.php?author=unknown'>unknown</a>";
            $retval = "unknown";
        }

        return $retval;
    }

    function getFactoidCreator()
    {
        return $this->_webifyAuthorString( $this->_created_by );
    }

    function getFactoidModifier()
    {
        return $this->_webifyAuthorString( $this->_modified_by );
    }

    function getCreatedDate()
    {
        return $this->_formatDate( $this->_created_time );
    }

    function getModifiedDate()
    {
        return $this->_formatDate( $this->_modified_time );
    }

    function _formatDate( $date )
    {
        return date("D M j G:i:s T Y", $date);
    }

    function getShortCreatedDate()
    {
        return $this->_formatShortDate( $this->_created_time );
    }

    function getShortModifiedDate()
    {
        return $this->_formatShortDate( $this->_modified_time );
    }

    function getShortHistoryDate()
    {
        if ($this->_history_time > 0) {
          return $this->_formatShortDate( $this->_history_time );
        }
        return "Current";
    }

    function getShortModifiedOrHistoryDate()
    {
        // take the newer of modified_time and history_time, remembering that
        // history time can lag by up to one day due to the snapshotting.
        if ($this->_modified_time > $this->_history_time - 86400) {
          return $this->_formatShortDate( $this->_modified_time );
        }
        return $this->_formatShortDate( $this->_history_time );
    }

    function _formatShortDate( $date )
    {
        return date("Y-m-d", $date);
    }


    function xrefCleaner($matches) {
        global $factoids;
        #echo $matches[1];
        switch ($matches[1]) {
            case "reply": return $matches[0];
            case "REPLY": return strtolower($matches[0]);
            case "action": return $matches[0];
            case "ACTION": return strtolower($matches[0]);
        }
        $u = html_entity_decode($matches[1]);
        $l = $matches[0];
        $xref = $factoids->getFactoidByKey($u);
        if ($xref) {
            $this->_xref_count++;
          # maybe one day...
      /*    return sprintf("<a href='factoid.php?key=%s' onmouseover='showVal(%s, %s)'>%s</a>",
                        urlencode($u),
                        urlencode($u),
                        urlencode($xref->getFactoidValue(false)),
                        $l
                    );*/
          return sprintf("<a href='factoid.php?key=%s'>%s</a>",
                        urlencode($u),
                        $l
                    );
        } else {
            $this->_broken_xref_count++;
            return "<span class='missingxref' title='Am I a missing cross-reference? If so, please fix me!'>$matches[0]</span>";
        }
    }

    function aliasCleaner($matches) {
        $this->_is_alias = true;
        $u = $matches[2];
        $l = $matches[2];
        $p = $matches[1];
        $u = urlencode(html_entity_decode($u));
        $l = htmlentities(html_entity_decode($l), ENT_QUOTES, "UTF-8");
        #echo $l;
        return "$p<a href='factoid.php?key=$u'>$l</a>";
    }


    function bugLinkCleaner($matches) {
        global $BUG_TRACKER_URL_PATTERN;
        $this->_bug_ref_count++;
        $bug = $matches[1];
        $bughref = sprintf($BUG_TRACKER_URL_PATTERN, $bug);
        return "<a href='$bughref'>#$bug</a>";
    }

}


class FactoidQuality {

  # approx max length of factoid is:
  #   protocol max - strlen(recipient nick) - strlen(bot nick)
  #        - strlen(sender nick) - strlen(' wants you to know: '
  #        - protocol overhead,whitespace
  # let's assume that: protocol max = 512 bytes
  #                    nicks are 12 chars (they can be up to 30 though!)
  #                    protocol overhead is 5 chars
  var $factoid_length_max = 451;
  var $factoid_length_warn = 400;

  var $_factoid;
  var $_factoid_checked = false;

  function __construct($factoid) {
    $this->_factoid = $factoid;
  }

  function lengthStyle() {
    $a = array('factoid_len_ok', 'factoid_len_long', 'factoid_len_huge');
    return $a[$this->badLength()];
  }

  function badLength() {
    $len = $this->_factoid->getFactoidLength(false);
    if ($len > $this->factoid_length_max) return 2;
    if ($len > $this->factoid_length_warn) return 1;
    return 0;
  }

  function hasBrokenXrefs() {
    if ($this->_factoid_checked) $this->_factoid->getFactoidValue();
      return ($this->_factoid->_broken_xref_count > 0);
  }

  function hasBadWords() {
    $badwords = "crap|suck|\blame\b|shit|fuck|cunt";
    return preg_match("/($badwords)/i",
              $this->_factoid->getFactoidValue(false))
       ||  preg_match("/($badwords)/i",
              $this->_factoid->_factoid_key);
  }

  # return a short description of the quality
  function short() {
    $s = array();
    $len = $this->badLength();
    if ($len) {
      $s[] = $len > 1 ? 'very&nbsp;long' : 'long';
    }
    if ($this->hasBrokenXrefs()) {
      $s[] = 'broken&nbsp;xrefs';
    }
    if ($this->hasBadWords()) {
      $s[] = 'content';
    }
    return join(', ', $s);
  }
}

?>
