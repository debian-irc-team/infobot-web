<?php
/*
 Copyright 2010-2012 Stuart Prescott <stuart@debian.org>

 infobot-web is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 infobot-web is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, see <http://www.gnu.org/licenses/>.
*/

require_once "inc/suggestions.php";
require_once "inc/factoidlist.php";

function showPropDelList($results, $format='html') {
  switch ($format) {
    case 'bot':
      showSuggestionListAsText($results, "no %s is \#del#\n");
      break;
    default:
      showFactoidListAsHtml($results, false);
  }
}

function showSuggestionList($results, $format='html') {
  switch ($format) {
    case 'bot':
      showFactoidListAsBotUpdates($results);
      break;
    default:
      showFactoidListAsHtml($results);
  }
}

function showDeleteListAsHtml($results) {
  echo "<table width='100%' class='searchresults'>
    <tr><th>Factoid&nbsp;Key</th><th>Factoid&nbsp;Value</th></tr>";

  foreach( $results as $factoid )
  {
    $key = $factoid->_factoid_key;
    //$mod_time = ($factoid->_modified_time|$factoid->_created_time);
    if ($factoid->_modified_time != "" || $factoid->_history_time > 0){
      $mod_time = "M: ".$factoid->getShortModifiedOrHistoryDate();
    } else {
      $mod_time = "C: ".$factoid->getShortCreatedDate();
    }
    $ukey = urlencode($key);
    $hkey = htmlentities($key, ENT_QUOTES, "UTF-8");
    $hmodtime = htmlentities($mod_time, ENT_QUOTES, "UTF-8");
    $hval = $factoid->getFactoidValue();
    echo "<tr>
          <td>
            <a href='factoid.php?key=$ukey'>$hkey</a><br />
            <span class='mtime'>$hmodtime</span>
          </td>
          <td>$hval</td>
        </tr>";
  }
  echo "</table><br />";
}

function showSuggestionListAsText($results, $format) {
  if (count($results)) {
    print "<pre>\n";
    foreach($results as $suggestion) {
      $key = htmlentities($suggestion->_factoid_key, ENT_QUOTES, "UTF-8");
      $value = htmlentities($suggestion->_suggested_value, ENT_QUOTES, "UTF-8");
      printf($format, $key, $value);
    }
    $type = $suggestion->_suggested_action;
    print "</pre>
          <hr />
          Once these have dealt with these changes, remove the suggestions
          by running:<br /><br />\n";
    printf("remove_suggestions %d %s", $_SERVER['REQUEST_TIME'], $type);
  } else {
    print "No suggestions found.";
  }
}
